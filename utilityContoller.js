({
    showProduts: function (component, event, helper) {
        component.set("v.numberOfProducts", event.getParam('count'));
        var utilityAPI = component.find("utilitybar");
        utilityAPI.setUtilityLabel(
            { label: 'New Label' });
    }
})